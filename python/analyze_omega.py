"""
On a straight
Alpha Force = sin(angle) * accel

"""

import glob
import os
import re

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

float_re = r'\s*(-?[\d\.]+)'

#os.chdir(r'E:\1stuff\Dropbox\dev\python\hwo2014-team-1421\python')
logfn = sorted(glob.glob('race_*.log'))[-1]
t = open(logfn).read()

def reject_outliers(data, m=2):
    return data[abs(data - np.mean(data)) < m * np.std(data)]

def main(text):
    print text[:100]
    varaa = re.findall('.*'.join([
        'V:' + float_re,
        'A:' + float_re,
        'radius:' + float_re,
        'angle:' + float_re,
        'angccel:' + float_re,
        ]),
        text)
    varaa = np.array(varaa).astype(float)
    if len(varaa) == 0:
        print 'empty crud'
        return

    def td():
        lowr = vro[(vro[:,0] < 13) & (abs(vro[:,1]) > 100) & (abs(vro[:,1]) < 9900) & (vro[:,2] > -30)]
        v, r, a, aa = zip(*lowr)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(v, r, o)

    #td()

    #rads = sorted(set(varaa[:,1]))
    rads = [9999]
    print('radiuses %s: %s' % (len(rads), rads))
    for r in rads:
        indexes = (varaa[:,2] == r) & (abs(varaa[:, 3]) > 3)
        data = varaa[indexes]
        plt.title('radius %s' % r)
        plt.grid(True)
        acc = data[:, 1]
        alpha = data[:, 3]
        aa = data[:,-1]
        f = np.sin(alpha) * acc
        #import pdb;pdb.set_trace()
        plt.scatter(aa, f)
        plt.show()

    plt.show()
    
for tr in t.split('track id'):
    main(tr)

