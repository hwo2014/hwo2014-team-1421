import collections
import pprint
import random
import datetime
import traceback
import json
import socket
import sys
import math
import logging
from collections import namedtuple


logging.basicConfig(filename='race_%s.log' % datetime.datetime.now().isoformat().replace(':', '-'), level=logging.DEBUG, filemode='w')
logging.debug('starting .py')

EPSILON = 0.01
SAFE_CAR_ANGLE = 30
INF = 9999
TOO_CLOSE_TO_OTHER_CAR = 200

ptv = namedtuple('point_target_velocity', 'start tv')
point = namedtuple('point', 'lane piece pdist')


def index_min(values):
    return min(xrange(len(values)),key=values.__getitem__)

def median(seq):
    return sorted(seq)[len(seq) // 2]
    
class SpanExtrap:
    "F: Angle Span -> Interpolated max speed in this span"
    
    def __init__(self, pairs):
        self.pairs = pairs
        self.factor = 1
        
    def val(self, x):
        if x <= self.pairs[0][0]:
            return self.pairs[0][1] * self.factor
        if x >= self.pairs[-1][0]:
            return self.pairs[-1][1] * self.factor

        for i, (x2, y2) in enumerate(self.pairs):
            if x < x2:
                x0, y0 = self.pairs[i - 1]
                span = (x2 - x0)
                percent = (x - x0) * 1.0 / span
                
                return (percent * y2 + (1 - percent) * y0) * self.factor


class NoobBot(object):

    def __init__(self, socket, name, key, track=None, carCount=1):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = track
        self.carCount = carCount
        print 'track: %s with %d cars' % (track, carCount)
        
        # TODO: simulate when our angle will hit 60 instead of this
        self.radius_to_tv = SpanExtrap([
            [15, 1.5],
            [30, 3],
            [60, 5],
            [90, 6.6],
            [200, 12],
            [1000, 99],
        ])
            
        
    def save_track_lengths(self, game_init_data):
        # tracks the radius of each piece in the track
        self.radii = {}
        
        # used to get a single number for the location on the track
        self.track_lengths = {}
        
        # total length of this lane
        self.lane_length = {}
        
        # same as track_lengths, but saves the start location of each track
        self.track_starts = {}
        # the lane data from the request
        self.lanes = game_init_data[u'race'][u'track'][u'lanes']
        # num of pieces in the track
        self.piece_count = len(game_init_data[u'race'][u'track'][u'pieces'])
        lane_count = len(self.lanes)
        # ?
        self.friction = -0.1  ## This changes with self.frictions
        self.pieces = game_init_data[u'race'][u'track'][u'pieces']
        for lane_obj in self.lanes:
            lane_index = lane_obj[u'index']
            lane_data = []
            lane_radii = []
            target_vs = []
            lane_starts = []
            total = 0
        
            for p in self.pieces:
                if u'length' in p:
                    length = p['length']
                    lane_radii.append(INF)
                elif u'angle' in p:
                    lane_radius = p['radius'] - lane_obj[u'distanceFromCenter'] * math.copysign(1, p['angle'])
                    length = abs(math.pi * lane_radius * p['angle'] * 2.0 / 360.0)
                    lane_radii.append(math.copysign(lane_radius, p['angle']))

                lane_data.append(length)
                lane_starts.append(total)
                total += length
            self.radii[lane_index] = lane_radii
            self.track_lengths[lane_index] = lane_data
            self.track_starts[lane_index] = lane_starts
            self.lane_length[lane_index] = sum(lane_data)
        
        logging.debug('lane lengths: %s' % self.lane_length)
        shortest_lane_length = self.lane_length[lane_index]
        self.shortest_lane_index = lane_index
        for lane_index, length in self.lane_length.items():
            if length < shortest_lane_length:
                shortest_lane_length = length
                self.shortest_lane_index = lane_index

        self.compute_target_vs()

    def compute_target_vs(self):
        self.target_vs = {}
        #self.etvs = {}
        for lane_obj in self.lanes:
            lane_index = lane_obj[u'index']
            lane_radii = self.radii[lane_index]
            #min_radius = min(lane_radii)
            #min_radius = 30
            #max_radius = max(lane_radii)
            #lowest_speed = 1
            #top_speed_bonus = 8
            # a = v^2 / r, is maybe relevant here
            
            # This track the maximum allowed speed in each piece in the lane
            self.target_vs[lane_index] = []
            for i, r in enumerate(lane_radii):
                r = abs(r)
                if r >= INF:
                    tv = INF
                else:
                    #tv = lowest_speed + top_speed_bonus * (1 - min_radius * 1.0 / r)
                    tv = self.radius_to_tv.val(r)
                self.target_vs[lane_index].append(tv)
                #x = self.distance_from_start(lane_index, i, 0)
                #vpoint = ptv(start=x, tv=tv)
                #self.target_vs[lane_index].append(vpoint)
            
            '''
            # compute effective target velocities
            cur_p = len(self.target_vs[lane_index]) - 1
            while cur_p > 0:
                start, tv = self.target_vs[lane_index][cur_p]
                prev_start, prev_tv = self.target_vs[lane_index][cur_p - 1]
                if prev_tv <= tv:
                    # no need to slow down ahead of time because we'll be going throttle(1)
                    # when we switch pieces
                    cur_p -= 1
                    continue
                
                # we have to slow down ahead of time
                '''
                


    def distance_from_start(self, lane, section, distance_in_section):
        return self.track_starts[lane][section] + distance_in_section
    
    def velocity(self):
        last_p = point(self.last_pos[u'lane'][u'endLaneIndex'], self.last_pos[u'pieceIndex'], self.last_pos[u'inPieceDistance'])
        cur_p = point(self.piece_pos[u'lane'][u'endLaneIndex'], self.piece_pos[u'pieceIndex'], self.piece_pos[u'inPieceDistance'])
        return self.distance(last_p, cur_p)
        #if self.piece_pos[u'inPieceDistance']) > self.last_pos[u'inPieceDistance']:
        #    return self.piece_pos[u'inPieceDistance']) - self.last_pos[u'inPieceDistance']
        #else:
        #    return
        
        #return self.distance_from_start(self.piece_pos[u'lane'][u'startLaneIndex'], self.piece_pos[u'pieceIndex'], self.piece_pos[u'inPieceDistance']) - \
        #       self.distance_from_start(self.last_pos[u'lane'][u'startLaneIndex'], self.last_pos[u'pieceIndex'], self.last_pos[u'inPieceDistance'])
        
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def startRace(self, track):
        self.msg("joinRace", {
          "botId": {
            "name": self.name,
            "key": self.key
          },
          "trackName": track,
          #"password": "schumi4ever",
          "carCount": self.carCount
        })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if self.track:
            self.debug = True
            self.startRace(self.track)
        else:
            self.debug = False
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.last_pos = self.piece_pos = {u'pieceIndex': 0, u'inPieceDistance': 0, u'lane': {u'startLaneIndex': 0, u'endLaneIndex': 0}}
        self.angles = collections.deque(maxlen=10)
        self.vs = collections.deque(maxlen=10)
        self.tvs = collections.deque(maxlen=10)
        self.angles.extend([0,0,0]) # initial values for calculating omega and angccel
        self.frictions = collections.deque(maxlen=16)
        self.last_tick = -1
        self.curr_velocity = 0
        self.dist = 0
        self.last_dist = 0
        self.lane_change_sent = False
        #self.a = []
        #self.v = []
        self.target_velocity = 6
        #self.ping()
        self.throttle(1)
    
    def convert_lane(self, src_point, dst_lane):
        percent_of_piece = self.track_lengths[src_point.lane][src_point.piece] / src_point.pdist
        target_lane_length = self.track_lengths[dst_lane][src_point.piece]
        dst_pdist = percent_of_piece * target_lane_length
        return point(dst_lane, src_point.piece, dst_pdist)
    
    def distance(self, point_a, point_b):
        """
        Compare distance from point_a to point_b
        Assumes point_b is farther ahead and guarantees the distance to be positive
        """
        if point_a.lane != point_b.lane:
            point_a = self.convert_lane(point_a, point_b.lane)
        
        dist = self.distance_from_start(*point_b)  - self.distance_from_start(*point_a)
        if dist < 0:
            dist += self.lane_length[point_a.lane]
        return dist

    def wrap_distance(self, lane, dist):
        if dist < 0:
            dist += self.lane_length[lane]
        return dist

    def is_going_too_fast(self, lane_index=None):
        """
        Go through all the pieces and check that if you throttle(0)
        from now on you'll be able to reach every piece at the right target velocity
        """
        if lane_index is None:
            lane_index = self.piece_pos[u'lane'][u'endLaneIndex']
        
        if self.curr_velocity < 0.01:
            return False
        
        for i, tv in enumerate(self.target_vs[lane_index]):
            if i == self.piece_index:
                continue
            if self.curr_velocity < tv:
                continue
                
            dist_to_piece = self.wrap_distance(lane_index, self.track_starts[lane_index][i] - self.dist)
            
            # if we break now
            time_to_piece = dist_to_piece / self.curr_velocity
            delta_v = time_to_piece * self.friction # negative number
                                                    # TODO: This assumes that deceleration is linear. I am not sure of that.
            v_at_piece = self.curr_velocity + delta_v
            if v_at_piece > tv:
                return True
        return False
            
        
    def on_car_positions(self, data):
        self.last_pos = self.piece_pos
        
        is_another_car_ahead = False
        for car in data:
            if car[u'id'][u'name'] == self.name:
                our_car = car
                break
        
        self.piece_pos = our_car['piecePosition']
        self.last_dist = self.dist
        l, i, d = self.piece_pos[u'lane'][u'startLaneIndex'], self.piece_pos[u'pieceIndex'], self.piece_pos[u'inPieceDistance']
        self.dist = self.distance_from_start(l, i, d)
        
        self.angle = our_car['angle']
        self.angles.append(self.angle)
        self.vs.append(self.curr_velocity)
        self.last_vel = self.curr_velocity
        #self.curr_velocity = self.dist - self.last_dist #self.velocity()
        self.curr_velocity = self.velocity()
        #self.v.append(self.curr_velocity)
        
        self.accel = self.curr_velocity - self.last_vel
        if self.accel < 0:
            self.frictions.append(self.accel)
            # `len(self.frictions) > 4` to avoid seeing one huge decceleration and 
            # acting on it
            if self.accel < self.friction and len(self.frictions) > 4:
                # stable modification of friction value
                new_fric = median(self.frictions)
                if new_fric < self.friction:
                    self.friction = (new_fric + self.friction) / 2.0
                
                # cautious 10% bump
                #self.friction = self.friction * 0.9 + self.accel * 0.1

        #self.a.append(self.accel)
        self.omega = self.angles[-1] - self.angles[-2]
        # angular acceleration
        self.angccel = self.omega - (self.angles[-2] - self.angles[-3])
        
        # make sure omega is opposite sign of angle
        angle_going_to_zero = self.angle * self.omega <= 0
        
        # TODO: endLaneIndex or startLaneIndex???
        lane = self.piece_pos[u'lane'][u'endLaneIndex']
        self.lane_index = lane
        self.piece_index = self.piece_pos[u'pieceIndex']
        #piece_percent = self.piece_pos[u'inPieceDistance'] / self.track_lengths[lane][piece_index]
        #start_v = self.target_vs[lane][piece_index]
        #end_v = self.target_vs[lane][(piece_index + 1) % self.piece_count]
        #self.target_velocity = start_v * (1 - piece_percent) + end_v * piece_percent
        
        self.target_velocity = self.target_vs[lane][self.piece_index]
        self.tvs.append(self.target_velocity)
        if self.curr_velocity * 1.0 / self.target_velocity > 1.2:
            self.can_lower_friction = True

        is_going_too_fast = self.is_going_too_fast()
        
        throttle = 0
        if not is_going_too_fast and (self.curr_velocity < self.target_velocity - EPSILON) and abs(self.angle) < SAFE_CAR_ANGLE:
            if abs(self.omega) < 1 or angle_going_to_zero:
                throttle = 1

        self.target_lane = self.find_better_lane(data, throttle)
        if self.target_lane != lane:
            self.lane_change_sent = True
            if lane < self.target_lane:
                self.msg("switchLane", "Right")
                logging.debug('>>switchLane Right')
            else:
                self.msg("switchLane", "Left")
                logging.debug('>>switchLane Left')
        else:
            self.throttle(throttle)
        
        if self.debug:
            tolog = dict(lane=l, piece=i, piece_dist=d, dist=self.dist,
                velocity=self.curr_velocity, accel=self.accel, angle=self.angle,
                tv=self.target_velocity, th=throttle,
                radius=self.radii[lane][self.piece_index],
                omega=self.omega,
                angccel=self.angccel,
                friction=self.friction,
                )
            line = 'P: {lane:<2}, {piece:<3}, {piece_dist:6.2f}\tL: {dist:6.2f}\tV: {velocity:6.2f}\tA: {accel:6.2f}\tth: {th}\ttv: {tv:6.2f}\tradius: {radius:<10}\tangle: {angle:6.2f}\tomega: {omega:6.2f}\tangccel: {angccel:6.3f}\tfric: {friction:6.2f}'.format(**tolog)
            logging.info(line)
        #print(line)
    
    def find_better_lane(self, data, throttle):
        """
        Returns the lane you want to go to.
        If you don't want to switch lanes = self.lane_index is returned
        """
        if 'switch' not in self.pieces[self.piece_pos[u'pieceIndex']]:
            return self.lane_index

        is_another_car_ahead = False
        for car in data:
            if car[u'id'][u'name'] != self.name:
                other_car_pos = car[u'piecePosition']
                if other_car_pos[u'lane'] == self.lane_index:
                    car_location = self.distance_from_start(self.lane_index, other_car_pos[u'pieceIndex'], other_car_pos[u'inPieceDistance'])
                    if self.wrap_distance(self.lane_index, car_location - self.dist) < TOO_CLOSE_TO_OTHER_CAR:
                        is_another_car_ahead = True
                        break
        
        if self.piece_pos[u'lane'][u'startLaneIndex'] != self.piece_pos[u'lane'][u'endLaneIndex']:
            # we're changing lanes
            self.lane_change_sent = False
            return self.lane_index
        
        if self.lane_change_sent:
            # we already fired a lane change command
            return self.lane_index
        
        if not is_another_car_ahead and self.shortest_lane_index == self.lane_index:
            return self.lane_index
        
        if throttle == 0:
            return self.lane_index
            # We don't want to accidentally change lanes when we're trying to slow down
            # and overwrite a breaking throttle command
            '''The server processes only one message per bot per tick, so if you, for example,
            want to switch lane, make sure not to send a throttle message for that tick before
            the switchLane message. If you send more than one message on a tick,
            the server will ignore all but the first one of those messages.
            '''
            # always switch inwards first, as the lanes are shorter there
            ## TODO: Will we always be going clockwise??
        
        if self.lane_index < self.shortest_lane_index:
            target = self.lane_index + 1
        else:
            target = self.lane_index - 1 % self.lane_count
        
        if self.is_going_too_fast(target):
            logging.debug('changing lanes would be too risky!')
            return self.lane_index
        else:
            return target
        
        
    def on_crash(self, data):
        if data['name'] == self.name:
            print("We crashed")

            if median(self.vs) / median(self.tvs) > 1.2:
                # we didn't slow down in time
                self.friction *= 0.8
            else:
                # we were speeding on purpose
                self.radius_to_tv.factor *= 0.8
            
            logging.error('------------- WE CRASHED -----------------')
            logging.error('------------- WE CRASHED -----------------')
            print self.radius_to_tv.factor
            logging.error('self.radius_to_tv.factor: %s' % self.radius_to_tv.factor)
            logging.error('------------- WE CRASHED -----------------')
            logging.error('------------- WE CRASHED -----------------')
        else:
            print("Someone else crashed")

        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
        
    def on_game_init(self, data):
        self.save_track_lengths(data)
        self.track_id = data['race']['track']['id']
        logging.info('\n--------------------- track id: %s ----------------------\n' % self.track_id)
        with open('track_' + self.track_id + '.log', 'w') as fhand:
            fhand.write(pprint.pformat(data))
        print self.track_id
        logging.info('T: ' + pprint.pformat(self.track_lengths))

    def msg_loop(self):
        self.msgs = []
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            try:
                msg = json.loads(line)
                self.msgs.append(msg)
                msg_type, data = msg['msgType'], msg['data']
                
                # handle carPositions specifically for the ticks
                if msg_type == 'carPositions':
                    if 'gameTick' in msg:
                        if self.last_tick >= int(msg['gameTick']):
                            print 'WARN: this tick(%s) is not new - last_tick(%s)' % (msg['gameTick'], self.last_tick)
                        self.last_tick = int(msg['gameTick'])
                        self.on_car_positions(data)
                    else:
                        print 'WARN: no tick in msg:', msg
                elif msg_type in msg_map:
                    msg_map[msg_type](data)
                else:
                    print 'Strange message - %s' % msg_type
                    logging.debug(str(msg))
                    self.ping()
            except Exception, e:
                print e
                traceback.print_exc()
            line = socket_file.readline()

def main(args, track=None, carCount=1):
    host, port, name, key = args
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*args))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, name, key, track, carCount)
    bot.run()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        main(sys.argv[1:5])